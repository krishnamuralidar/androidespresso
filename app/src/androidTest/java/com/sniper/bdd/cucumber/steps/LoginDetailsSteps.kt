package com.sniper.bdd.cucumber.steps

import android.support.test.rule.ActivityTestRule
import com.sniper.bdd.LoginActivity
import com.sniper.bdd.cucumber.espresso.login.LoginScreenRobot
import com.sniper.bdd.utils.ActivityFinisher
import cucumber.api.java.After
import cucumber.api.java.Before
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class LoginDetailsSteps {

    private val robot = LoginScreenRobot()

    private val activityRule = ActivityTestRule(LoginActivity::class.java, false, false)

    @Before
    fun setup() {
        //not needed now, but you may be needed to setup mock responses before your screen starts
    }

    @After
    fun tearDown() {
        ActivityFinisher.finishOpenActivities() // Required for test scenarios with multiple activities or scenarios with more cases
    }

    @Given("^Launch the app$")
    fun launch_the_app() {
        robot.launchLoginScreen(activityRule)
    }

    @When("^User clicks email field$")
    fun user_clicks_email_field() {
        robot.selectEmailField()
    }

    @And("^User closes the keyboard$")
    fun user_closes_the_keyboard() {
        robot.closeKeyboard()
    }

    @And("^User enter valid email (\\S+)$")
    fun user_enters_valid_email(email: String) {
        robot.enterEmail(email)
    }

    @And("^User clicks password field$")
    fun user_clicks_password_field() {
        robot.selectPasswordField()
    }

    @And("^User enters valid password (\\S+)$")
    fun user_enters_valid_password(password: String) {
        robot.enterPassword(password)
    }

    @And("^User click sign in button$")
    fun user_click_sign_in_button() {
        robot.clickSignInButton()
    }

    @Then("^User should  see successful login message$")
    fun user_should_see_successful_login_message() {
        robot.isSuccessfulLogin()
    }


}
