Feature: Enter login details

  @smoke
    @e2e
  Scenario Outline: Verify Login

    Given Launch the app
    When User clicks email field
    And User enter valid email <email>
    And User closes the keyboard
    And User clicks password field
    And User enters valid password <password>
    And User closes the keyboard
    And User click sign in button
    Then User should  see successful login message

    Examples:
      | email              | password     |
      | abc@aldi-suid.com  | password123  |
      | test@aldi-suid.com | testpassword |


  @[CU]CROSS-1509
  Feature: Xray integration with automated tests

	#<p>&nbsp;Test to Integrate Xray with Automated test</p>
	#
  @[CU]CROSS-1508
  Scenario: Sample tests to Integrate Xray with Automated test
    Given : Launch mobile app
    Then : Verify the Home button